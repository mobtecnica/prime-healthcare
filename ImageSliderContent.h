//
//  ImageSliderContent.h
//  Prime Healthcare
//
//  Created by macbook pro on 8/28/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageSliderContent : UIView
+(ImageSliderContent *)initWithBundlewithImage:(NSString *)image;
@property (weak, nonatomic) IBOutlet UIImageView *imageHospital;
@property(nonatomic,assign) NSInteger itemTag;
@property (weak, nonatomic) IBOutlet UIView *baseView;
@end

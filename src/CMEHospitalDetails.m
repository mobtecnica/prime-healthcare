//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.5.8.1
//
// Created by Quasar Development at 19/08/2016
//
//---------------------------------------------------


#import "CMEHelper.h"
#import "CMEHospitalDetails.h"


@implementation CMEHospitalDetails
@synthesize ClinicId;
@synthesize ClinicName;
@synthesize ContactInfo;
@synthesize DepartmentName;
@synthesize HospitalPhoto;
@synthesize LatitudeLocation;
@synthesize LongtitudeLocation;
@synthesize RegistrationTimings;
@synthesize StreetName;
@synthesize TimingsOnFridays;
@synthesize TimingsOnSaturdays;
@synthesize TimingsOnWeekdays;

+ (CMEHospitalDetails *)createWithXml:(DDXMLElement *)__node __request:(CMERequestResultHandler*) __request
{
    if(__node == nil) { return nil; }
    CMEHospitalDetails* obj = [[self alloc] init];
    [obj loadWithXml: __node __request:__request];
    return obj;
}

-(id)init {
    if ((self=[super init])) {
    }
    return self;
}

- (void) loadWithXml: (DDXMLElement*) __node __request:(CMERequestResultHandler*) __request{
    DDXMLNode* node=__node;
    for(int i=0;i< node.childCount;i++)
    {
        DDXMLNode* node=[__node childAtIndex:i];
        if(node.kind==DDXMLElementKind)
        {
            DDXMLElement* __node=(DDXMLElement*)node;
             if([__node.localName isEqualToString:@"ClinicId"])
             {
                if([CMEHelper isValue:__node name:@"ClinicId"])
                {
                    self.ClinicId = [NSNumber numberWithInt:[[__node stringValue] intValue]];
                }
                continue;
             }
             if([__node.localName isEqualToString:@"ClinicName"])
             {
                if([CMEHelper isValue:__node name:@"ClinicName"])
                {
                    self.ClinicName = [__node stringValue];
                }
                continue;
             }
             if([__node.localName isEqualToString:@"ContactInfo"])
             {
                if([CMEHelper isValue:__node name:@"ContactInfo"])
                {
                    self.ContactInfo = [__node stringValue];
                }
                continue;
             }
             if([__node.localName isEqualToString:@"DepartmentName"])
             {
                if([CMEHelper isValue:__node name:@"DepartmentName"])
                {
                    self.DepartmentName = [__node stringValue];
                }
                continue;
             }
             if([__node.localName isEqualToString:@"HospitalPhoto"])
             {
                if([CMEHelper isValue:__node name:@"HospitalPhoto"])
                {
                    self.HospitalPhoto = [__node stringValue];
                }
                continue;
             }
             if([__node.localName isEqualToString:@"LatitudeLocation"])
             {
                if([CMEHelper isValue:__node name:@"LatitudeLocation"])
                {
                    self.LatitudeLocation = [__node stringValue];
                }
                continue;
             }
             if([__node.localName isEqualToString:@"LongtitudeLocation"])
             {
                if([CMEHelper isValue:__node name:@"LongtitudeLocation"])
                {
                    self.LongtitudeLocation = [__node stringValue];
                }
                continue;
             }
             if([__node.localName isEqualToString:@"RegistrationTimings"])
             {
                if([CMEHelper isValue:__node name:@"RegistrationTimings"])
                {
                    self.RegistrationTimings = [__node stringValue];
                }
                continue;
             }
             if([__node.localName isEqualToString:@"StreetName"])
             {
                if([CMEHelper isValue:__node name:@"StreetName"])
                {
                    self.StreetName = [__node stringValue];
                }
                continue;
             }
             if([__node.localName isEqualToString:@"TimingsOnFridays"])
             {
                if([CMEHelper isValue:__node name:@"TimingsOnFridays"])
                {
                    self.TimingsOnFridays = [__node stringValue];
                }
                continue;
             }
             if([__node.localName isEqualToString:@"TimingsOnSaturdays"])
             {
                if([CMEHelper isValue:__node name:@"TimingsOnSaturdays"])
                {
                    self.TimingsOnSaturdays = [__node stringValue];
                }
                continue;
             }
             if([__node.localName isEqualToString:@"TimingsOnWeekdays"])
             {
                if([CMEHelper isValue:__node name:@"TimingsOnWeekdays"])
                {
                    self.TimingsOnWeekdays = [__node stringValue];
                }
                continue;
             }
        }
     }
}

-(void) serialize:(DDXMLElement*)__parent __request:(CMERequestResultHandler*) __request
{

            
    DDXMLElement* __ClinicIdItemElement=[__request writeElement:ClinicId type:[NSNumber class] name:@"ClinicId" URI:@"http://schemas.datacontract.org/2004/07/Prime_Service.Repository" parent:__parent skipNullProperty:YES];
    if(__ClinicIdItemElement!=nil)
    {
        [__ClinicIdItemElement setStringValue:[self.ClinicId stringValue]];
    }
            
    DDXMLElement* __ClinicNameItemElement=[__request writeElement:ClinicName type:[NSString class] name:@"ClinicName" URI:@"http://schemas.datacontract.org/2004/07/Prime_Service.Repository" parent:__parent skipNullProperty:YES];
    if(__ClinicNameItemElement!=nil)
    {
        [__ClinicNameItemElement setStringValue:self.ClinicName];
    }
            
    DDXMLElement* __ContactInfoItemElement=[__request writeElement:ContactInfo type:[NSString class] name:@"ContactInfo" URI:@"http://schemas.datacontract.org/2004/07/Prime_Service.Repository" parent:__parent skipNullProperty:YES];
    if(__ContactInfoItemElement!=nil)
    {
        [__ContactInfoItemElement setStringValue:self.ContactInfo];
    }
            
    DDXMLElement* __DepartmentNameItemElement=[__request writeElement:DepartmentName type:[NSString class] name:@"DepartmentName" URI:@"http://schemas.datacontract.org/2004/07/Prime_Service.Repository" parent:__parent skipNullProperty:YES];
    if(__DepartmentNameItemElement!=nil)
    {
        [__DepartmentNameItemElement setStringValue:self.DepartmentName];
    }
            
    DDXMLElement* __HospitalPhotoItemElement=[__request writeElement:HospitalPhoto type:[NSString class] name:@"HospitalPhoto" URI:@"http://schemas.datacontract.org/2004/07/Prime_Service.Repository" parent:__parent skipNullProperty:YES];
    if(__HospitalPhotoItemElement!=nil)
    {
        [__HospitalPhotoItemElement setStringValue:self.HospitalPhoto];
    }
            
    DDXMLElement* __LatitudeLocationItemElement=[__request writeElement:LatitudeLocation type:[NSString class] name:@"LatitudeLocation" URI:@"http://schemas.datacontract.org/2004/07/Prime_Service.Repository" parent:__parent skipNullProperty:YES];
    if(__LatitudeLocationItemElement!=nil)
    {
        [__LatitudeLocationItemElement setStringValue:self.LatitudeLocation];
    }
            
    DDXMLElement* __LongtitudeLocationItemElement=[__request writeElement:LongtitudeLocation type:[NSString class] name:@"LongtitudeLocation" URI:@"http://schemas.datacontract.org/2004/07/Prime_Service.Repository" parent:__parent skipNullProperty:YES];
    if(__LongtitudeLocationItemElement!=nil)
    {
        [__LongtitudeLocationItemElement setStringValue:self.LongtitudeLocation];
    }
            
    DDXMLElement* __RegistrationTimingsItemElement=[__request writeElement:RegistrationTimings type:[NSString class] name:@"RegistrationTimings" URI:@"http://schemas.datacontract.org/2004/07/Prime_Service.Repository" parent:__parent skipNullProperty:YES];
    if(__RegistrationTimingsItemElement!=nil)
    {
        [__RegistrationTimingsItemElement setStringValue:self.RegistrationTimings];
    }
            
    DDXMLElement* __StreetNameItemElement=[__request writeElement:StreetName type:[NSString class] name:@"StreetName" URI:@"http://schemas.datacontract.org/2004/07/Prime_Service.Repository" parent:__parent skipNullProperty:YES];
    if(__StreetNameItemElement!=nil)
    {
        [__StreetNameItemElement setStringValue:self.StreetName];
    }
            
    DDXMLElement* __TimingsOnFridaysItemElement=[__request writeElement:TimingsOnFridays type:[NSString class] name:@"TimingsOnFridays" URI:@"http://schemas.datacontract.org/2004/07/Prime_Service.Repository" parent:__parent skipNullProperty:YES];
    if(__TimingsOnFridaysItemElement!=nil)
    {
        [__TimingsOnFridaysItemElement setStringValue:self.TimingsOnFridays];
    }
            
    DDXMLElement* __TimingsOnSaturdaysItemElement=[__request writeElement:TimingsOnSaturdays type:[NSString class] name:@"TimingsOnSaturdays" URI:@"http://schemas.datacontract.org/2004/07/Prime_Service.Repository" parent:__parent skipNullProperty:YES];
    if(__TimingsOnSaturdaysItemElement!=nil)
    {
        [__TimingsOnSaturdaysItemElement setStringValue:self.TimingsOnSaturdays];
    }
            
    DDXMLElement* __TimingsOnWeekdaysItemElement=[__request writeElement:TimingsOnWeekdays type:[NSString class] name:@"TimingsOnWeekdays" URI:@"http://schemas.datacontract.org/2004/07/Prime_Service.Repository" parent:__parent skipNullProperty:YES];
    if(__TimingsOnWeekdaysItemElement!=nil)
    {
        [__TimingsOnWeekdaysItemElement setStringValue:self.TimingsOnWeekdays];
    }


}
@end

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.5.8.1
//
// Created by Quasar Development at 23/08/2016
//
//---------------------------------------------------


#import <Foundation/Foundation.h>
    
@interface ASTSoapError : NSError

@property (retain,nonatomic) id Details;
    
-(id)initWithDetails:(NSString*) faultString details:(id)details;
@end

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.5.8.1
//
// Created by Quasar Development at 19/08/2016
//
//---------------------------------------------------


#import <Foundation/Foundation.h>
    
@interface CMESoapError : NSError

@property (retain,nonatomic) id Details;
    
-(id)initWithDetails:(NSString*) faultString details:(id)details;
@end

//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.5.8.1
//
// Created by Quasar Development at 19/08/2016
//
//---------------------------------------------------


#import <Foundation/Foundation.h>

#import "CMERequestResultHandler.h"
#import "DDXML.h"



@interface CMEUserRegistrationVerification : NSObject <CMEISerializableObject>


@property (retain,nonatomic,getter=getDeviceId) NSString* DeviceId;

@property (retain,nonatomic,getter=getFCMToken) NSString* FCMToken;

@property (retain,nonatomic,getter=getMessage) NSString* Message;

@property (retain,nonatomic,getter=getMobileNo) NSString* MobileNo;

@property (retain,nonatomic,getter=getOTPPassword) NSString* OTPPassword;

@property (retain,nonatomic,getter=getPassword) NSString* Password;

@property (retain,nonatomic,getter=getRGNo) NSString* RGNo;

@property (retain,nonatomic,getter=getResult) NSNumber* Result;
-(id)init;
-(void)loadWithXml: (DDXMLElement*)__node __request:(CMERequestResultHandler*) __request;
+(CMEUserRegistrationVerification*) createWithXml:(DDXMLElement*)__node __request:(CMERequestResultHandler*) __request;
-(void) serialize:(DDXMLElement*)__parent __request:(CMERequestResultHandler*) __request;
@end
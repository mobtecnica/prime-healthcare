//
//  PUtilities.h
//  Prime Healthcare
//
//  Created by macbook pro on 9/15/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PUtilities : NSObject

+(void)setUserInfo:(NSDictionary *)userInfo;
+(NSDictionary *)getUserInfo;
+(NSString *)getName;
+(NSString *)getRegId;
+(NSString *)getMobile;
+(NSString *)getpassword;
@end

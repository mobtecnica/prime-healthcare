//
//  ImageSliderContent.m
//  Prime Healthcare
//
//  Created by macbook pro on 8/28/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "ImageSliderContent.h"

@implementation ImageSliderContent

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+(ImageSliderContent *)initWithBundlewithImage:(NSString *)image{
    NSArray *array=[[NSBundle mainBundle]loadNibNamed:@"ImageSlider" owner:self options:nil];
    
    ImageSliderContent *view=[array objectAtIndex:0];
    view.imageHospital.image=[UIImage imageNamed:image];
    view.baseView.layer.cornerRadius=7;
    view.baseView.clipsToBounds=YES;
    return view;
}
@end

//
//  FutureAppointments+CoreDataProperties.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 30/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "FutureAppointments+CoreDataProperties.h"

@implementation FutureAppointments (CoreDataProperties)

@dynamic appointmentDate;
@dynamic branchName;
@dynamic doctorName;
@dynamic doctorPhoto;
@dynamic qualification;
@dynamic speciality;

@end

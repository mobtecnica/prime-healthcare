//
//  FutureAppointmentsViewController.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 30/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "FutureAppointmentsViewController.h"
#import "Header.h"
#import "FutureInfoTableViewCell.h"


@interface FutureAppointmentsViewController ()<UITableViewDataSource, UITableViewDelegate, EDRSoapServiceResponse>{
    NSArray *dataArray;
    BOOL isLoaded;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation FutureAppointmentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tableView.rowHeight = UITableViewAutomaticDimension;
    _tableView.estimatedRowHeight = 200.0f;
    self.navigationItem.leftBarButtonItem = [self setUpBackButton];
    self.title = @"My Information";
    [self getAllAppontments];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -TableView Delegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if ([dataArray count]>0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.tableView.backgroundView = [[UIView alloc]initWithFrame:CGRectZero];
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        });
        
        return 1;
        
    } else {
        // Display a message when the table is empty
        if (isLoaded) {
            UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, self.tableView.bounds.size.height)];
            
            messageLabel.text = @"No record is  available.";
            messageLabel.textColor = UIColorFromRGB(0xE85206);
            messageLabel.numberOfLines = 0;
            messageLabel.textAlignment = NSTextAlignmentCenter;
            messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
            [messageLabel sizeToFit];
            self.tableView.backgroundView = messageLabel;
            self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        }
    }
    
    return 0;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier=@"futureCell";
    
    FutureInfoTableViewCell *cell=(FutureInfoTableViewCell *) [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell==nil) {
        //Set Default Properties
        cell = [[FutureInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    FutureAppointments *appointment = [dataArray objectAtIndex:indexPath.row];
    
    cell.doctorNameLabel.text = appointment.doctorName;
    cell.qualificationLabel.text = appointment.qualification;
    cell.specializationLabel.text = appointment.speciality;
    cell.branchLabel.text = appointment.branchName;
    cell.dateLabel.text = [appointment.appointmentDate dateStringfromDateTime];
    cell.timeLabel.text = [appointment.appointmentDate timeStringfromDateTime];
    NSLog(@"%@",appointment.doctorPhoto);
    [cell.doctorProfilePhoto sd_setImageWithURL:[NSURL URLWithString:appointment.doctorPhoto] placeholderImage:[UIImage imageNamed:@"placeholder_square"]];
    return cell;
    
}

#pragma mark - networking
- (void)getAllAppointmentDetailsFromServer{
   [SVProgressHUD showWithStatus:@"Loading..."];
    EDRBasicHttpBinding_IPrimeAppService* service = [[EDRBasicHttpBinding_IPrimeAppService alloc]init];
    service.EnableLogging=TRUE;
    [service GetPatientFutureAppointmentDetailsAsync:[PUtilities getRegId] __target:self];
    
}

- (void)getAllAppontments{
    dataArray = [FutureAppointments getAllFutureAppointmentsDetails];
    if (dataArray.count > 0) {
        [_tableView reloadData];
    }
    else{
        [self getAllAppointmentDetailsFromServer];
    }
}


#pragma mark -  response handler methods
- (void)onSuccess:(id)value methodName:(NSString *)methodName{
    if([value isKindOfClass:[EDRArrayOfFutureAppointmentDetails class]])
    {
        EDRArrayOfFutureAppointmentDetails* res=(EDRArrayOfFutureAppointmentDetails*)value;
        //in res variable you have a value returned from your web service
        if (res.count > 0) {
            [FutureAppointments deleteAllFutureAppointmentsList];
            for (EDRFutureAppointmentDetails *appointment in res) {
                [FutureAppointments saveFutureAppointment:appointment];
            }
            dataArray = [FutureAppointments getAllFutureAppointmentsDetails];
            [_tableView reloadData];
        }
        isLoaded = YES;
        [_tableView reloadData];
        [SVProgressHUD dismiss];
        
    }
    else
    {
        isLoaded = YES;
        [_tableView reloadData];
        [SVProgressHUD dismiss];
        //here you can put the code you want to run when your web service operation is finished
        [self showAlert:@"Alert!" message:@"No records available!"];
    }
}

- (void)onError:(NSError *)error{
    isLoaded = YES;
    [_tableView reloadData];
    [SVProgressHUD dismiss];
    [self showAlert:@"Error!" message:error.localizedDescription];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

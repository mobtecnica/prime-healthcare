//
//  LoginViewController.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 22/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"

@class LoginViewController;
@protocol LoginViewControllerDelegate<NSObject>

@optional
- (void)didClickSignupButton:(LoginViewController *)viewController;
- (void)didClickSkipButton;
- (void)didClickForgotPasswordBtn:(LoginViewController *)vc;
- (void)loginCompletedSuccessfully;
@end


@interface LoginViewController : BaseViewController

@property (assign, nonatomic) id <LoginViewControllerDelegate>delegate;

@end

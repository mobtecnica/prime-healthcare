//
//  ForgotPasswordViewController.m
//  Prime Healthcare
//
//  Created by macbook pro on 8/22/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "Header.h"
#import "UINavigationBar+Awesome.h"
#import "CMEServiceResult.h"
#import "CMEArrayOfServiceResult.h"
#define K_REGISTERATION @"1"
#define K_FORGOT_PASSWORD @"0"
@interface ForgotPasswordViewController ()<CMESoapServiceResponse,UITextFieldDelegate,ATKSoapServiceResponse>
@property (weak, nonatomic) IBOutlet UITextField *otpFeild;
@property (weak, nonatomic) IBOutlet UITextField *newpassword;
@property (weak, nonatomic) IBOutlet UITextField *confirmpassword;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollcontentheight;
//@property(nonatomic,strong) NSString *otpPassword;
@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setAutomaticallyAdjustsScrollViewInsets:NO];
    // Do any additional setup after loading the view from its nib.
    _newpassword.delegate=self;
    _confirmpassword.delegate=self;
    [self setUpView];
    self.navigationItem.leftBarButtonItem =  [self setUpBackButton];
    
   
}
-(void)viewDidAppear:(BOOL)animated{

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillDisappear:(BOOL)animated{

 
    [self setStatusBarBackgroundColor:[UIColor clearColor]];
    



}
-(void)viewDidDisappear:(BOOL)animated{
    [_otpFeild setText:nil];
    [_newpassword setText:nil];
    [_confirmpassword setText:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"it is working");
    //if type is K_FORGOT_PASSWORD - we are passing the otp from previuous VC to this VC
    if ([_TYPE isEqualToString: K_REGISTERATION]) {
        [self getOTP];
        
    }
}
#pragma mark - SETUP VIEW
-(void)setUpView{
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self setStatusBarBackgroundColor:[UIColor clearColor]];

    self.view.layer.cornerRadius=2.0;
    self.view.clipsToBounds=YES;
    _submitBtn.layer.cornerRadius=2.0;
   _submitBtn.clipsToBounds=YES;
    _profileImage.layer.cornerRadius=_profileImage.frame.size.height/2;
    _profileImage.clipsToBounds=YES;
    _nameLabel.text=_selectedPatient.PatientName;
    
    //addinng orange layer
    CALayer *borderLayer = [CALayer layer];
    CGRect borderFrame = CGRectMake(0, 0, (_profileImage.frame.size.width), (_profileImage.frame.size.height));
    [borderLayer setBackgroundColor:[[UIColor clearColor] CGColor]];
    [borderLayer setFrame:borderFrame];
    [borderLayer setCornerRadius:_profileImage.layer.cornerRadius];
    [borderLayer setBorderWidth:1];
    [borderLayer setBorderColor:[[UIColor orangeColor] CGColor]];
    [_profileImage.layer addSublayer:borderLayer];
    
    
}

- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}


#pragma mark- Other funtions
-(void)getOTP{
    [SVProgressHUD showWithStatus:@"Sending OTP..."];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CMEBasicHttpBinding_IPrimeAppService *service=[CMEBasicHttpBinding_IPrimeAppService new];
        [service AddVerificationDetailsAsync:_selectedPatient.RGNo OTPPassword:@"" TransType:@"ADD_VERIFICATION_DETAILS" __target:self];
    });
    

}
-(void)authenticateUser{
    [SVProgressHUD showWithStatus:@"registering..."];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *fcmToken=[[NSUserDefaults standardUserDefaults] valueForKey:@"FCM_TOKEN"];
        NSString *uuid = [[NSUUID UUID] UUIDString];
        uuid = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
        ATKBasicHttpBinding_IPrimeAppService* service = [[ATKBasicHttpBinding_IPrimeAppService alloc]init];
        
        [service RegisterMobileAppUserAsync:uuid FCMToken:fcmToken DeviceType:@"1" UserName:_selectedPatient.RGNo Password:_newpassword.text MobileNo:_selectedPatient.MobileNo PatientName:_selectedPatient.PatientName TransType:@"ADD_MOBILE_APP_USER" __target:self];
        
    });
 
 }
- (IBAction)submitAction:(id)sender {
    
    [self.view endEditing:YES];
    if ([_otpFeild.text isEqualToString:@""]) {
        [self showAlert:@"Error!" message:@"Enter the OTP received"];
        return;
    }
    if ([_newpassword.text isEqualToString:@""]||[_confirmpassword.text isEqualToString:@""]) {
        [self showAlert:@"Error!" message:@"Enter Password"];
        return;
    }
    if (![_newpassword.text isEqualToString:_confirmpassword.text]) {
        [self showAlert:@"Error!" message:@"Password missmatch"];
        return;
    }
    if (_otpRecieved!=_otpFeild.text) {
        [self showAlert:@"Error!" message:@"Incorrect OTP"];
        return;
    }
    if ([self isInternetConnection]) {
        
        if ([_TYPE isEqualToString:K_FORGOT_PASSWORD]) {
            
            
            //dummy values
            CMEBasicHttpBinding_IPrimeAppService *service=[CMEBasicHttpBinding_IPrimeAppService new];
            [service UpdatePasswordAsync:_regNumber OTPPassword:_newpassword.text TransType:@"UPDATE_PASSWORD" __target:self];
            return;
            
        }
        if ([_TYPE isEqualToString:K_REGISTERATION]) {
            
            [self authenticateUser];
        }
        
    }
    else{
        [self showNoInternetAlert];
    }
}

-(void)onSuccess:(id)value methodName:(NSString *)methodName{
    NSLog(@"%@",value);
    [SVProgressHUD dismiss];
    dispatch_async(dispatch_get_main_queue(), ^{

    if ([methodName isEqualToString:@"AddVerificationDetails"]) {
        CMEArrayOfServiceResult *result=(CMEArrayOfServiceResult *)value;
        CMEServiceResult *verificationDetails=(CMEServiceResult *)[result.items objectAtIndex:0];
        _otpRecieved =verificationDetails.otpPassword;
        NSLog(@"OTP Pasword:  %@",_otpRecieved);
    }
    
    if ([methodName isEqualToString:@"RegisterMobileAppUser"]) {
            NSLog(@"%@",value);
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"DID_SKIP_PRESSED"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        // navigate to home screen
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        UIViewController *mainViewController = [storyboard instantiateViewControllerWithIdentifier:@"homeView"];
        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:mainViewController];
        navigationController.viewControllers = controllers;
        
        [self showAlert:@"Registration success!" message:@"Please login to continue"];
    }
    });
}
-(void)onError:(NSError *)error{
    NSLog(@"%@",error);
    [SVProgressHUD dismiss];
    [self showAlert:@"Alert" message:[error localizedDescription]];

}
#pragma text feild delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength < 7;
}

@end

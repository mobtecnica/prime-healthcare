//
//  MedicationTableViewCell.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 01/10/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MedicationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *medicinename;

@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;
@property (weak, nonatomic) IBOutlet UIView *baseView;
@end

//
//  DoctorProfileViewController.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 26/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"
#import "CMEDoctorDetails.h"
#import "FavoriteDoctor.h"

@interface DoctorProfileViewController : BaseViewController

- (IBAction)readmoreAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableview;

@property(weak,nonatomic)FavoriteDoctor *doctor;
@end

//
//  FeedbackViewController.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 01/10/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "FeedbackViewController.h"
#import "Header.h"
#import "UMGPatientFeedbackDetails.h"

@interface FeedbackViewController (){
    
    UserProfile *profile;
    NSNumber *patientVisitId;
    NSNumber *feedbackId;
    NSMutableArray *questionsArray;
    NSMutableArray *answersArray;
    NSMutableArray *displayedQuestions;
    NSMutableArray *buttonArray;
    NSMutableDictionary *currentQuestion;
    NSMutableDictionary *previousQuestion;
}

@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UIButton *answerButton1;
@property (weak, nonatomic) IBOutlet UIButton *answerButton2;
@property (weak, nonatomic) IBOutlet UIButton *answerButton3;
@property (weak, nonatomic) IBOutlet UIButton *answerButton4;
@property (weak, nonatomic) IBOutlet UIView *multipleChoiceView;
@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UITextView *commentField;
@property (weak, nonatomic) IBOutlet UILabel *commentQuestionLabel;
@property (weak, nonatomic) IBOutlet UIView *thankYouView;
@property (weak, nonatomic) IBOutlet UIButton *finishButton;

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.leftBarButtonItem = [self setUpBackButton];
    self.title = @"Past Visit";
//   AddPatientFeedbackDetails
    profile = [UserProfile getUserProfile];
    
    
    [self setupView];
   
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showWithStatus:@"Loading..."];
//        [self.view bringSubviewToFront:SVProgressHUD];
    });
    answersArray = [[NSMutableArray alloc] init];
    displayedQuestions = [[NSMutableArray alloc] init];
    // long-running webservice
    [self addPatientFeedbackDetails];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setupView{
    // style buttons
    _answerButton1.layer.cornerRadius = 3.0f;
    _answerButton1.clipsToBounds = YES;
    
    _answerButton2.layer.cornerRadius = 3.0f;
    _answerButton2.clipsToBounds = YES;
    
    _answerButton3.layer.cornerRadius = 3.0f;
    _answerButton3.clipsToBounds = YES;
    
    _answerButton4.layer.cornerRadius = 3.0f;
    _answerButton4.clipsToBounds = YES;
    
    _submitButton.layer.cornerRadius = 3.0f;
    _submitButton.clipsToBounds = YES;
    
    _finishButton.layer.cornerRadius = 3.0f;
    _finishButton.clipsToBounds = YES;
    
    _multipleChoiceView.hidden = YES;
    _commentView.hidden = YES;
    _thankYouView.hidden = YES;
    
    buttonArray = [[NSMutableArray alloc] init];
    [buttonArray addObject:_answerButton1];
    [buttonArray addObject:_answerButton2];
    [buttonArray addObject:_answerButton3];
    [buttonArray addObject:_answerButton4];
}

-(void)back:(UIBarButtonItem *)btn{
    
    if (answersArray.count <= 1) {
        [self showSurveyCompletedAlert:@"Exit feedback?" cancel:YES];
    }
    else{
        NSArray *answers = [currentQuestion objectForKey:@"answers"];
        [displayedQuestions removeObjectsInArray:answers];
        previousQuestion = [answersArray objectAtIndex:answersArray.count -2];
        NSArray *previousArray = [previousQuestion objectForKey:@"answers"];
        [displayedQuestions removeObjectsInArray:previousArray];
        [answersArray removeObjectsInArray:@[currentQuestion,previousQuestion]];
        [self displayQuestions];
        
    }
    
}


- (void)addPatientFeedbackDetails{
    
    
    
    UMGBasicHttpBinding_IPrimeAppService* service = [[UMGBasicHttpBinding_IPrimeAppService alloc]init];
    [service AddPatientFeedbackDetailsAsyncWithBlock:[PUtilities getRegId] PatientName:profile.patientName MobileNo:profile.mobileNo EmailId:profile.email TransType:@"ADD" __handler:^(id obj) {
        
        if ([obj isKindOfClass:[UMGArrayOfPatientFeedbackDetails class]]) {
            
            UMGArrayOfPatientFeedbackDetails *res = (UMGArrayOfPatientFeedbackDetails *)obj;
            UMGPatientFeedbackDetails *feedBackDetails = [res.items firstObject];
            
            if ([feedBackDetails.Result integerValue] == [SUCCESS_RESPONCE integerValue]) {
                feedbackId = feedBackDetails.FeedbackId;
                [self addPatientVisitDetails:feedBackDetails.FeedbackId];
                 
            }
        }
        
    }];
}

- (void)addPatientVisitDetails:(NSNumber *)feedbackID{
    
    UMGBasicHttpBinding_IPrimeAppService* service = [[UMGBasicHttpBinding_IPrimeAppService alloc]init];
    NSString *deviceID=[[NSUserDefaults standardUserDefaults]valueForKey:@"DEVICE_ID"];
    [service AddPatientVisitDetailsAsyncWithBlock:feedbackID DateOfVisit:_visit.visitDate TimeOfVisit:[_visit.visitDate timeStringfromDateTime] DepartmentName:_visit.speciality DoctorName:_visit.doctorName LanguageId:@1 LocationId:@14 DeviceId:deviceID BranchId:[NSNumber numberWithInt:[_visit.branchId intValue]] VisitId:[NSNumber numberWithInt:[_visit.visitId intValue]] SubLocationId:@19 SurveyByMobileApp:@1 TransType:@"ADD" __handler:^(id obj) {

        UMGArrayOfPatientFeedbackDetails *res = (UMGArrayOfPatientFeedbackDetails *)obj;
        UMGPatientFeedbackDetails *feedBackDetails = [res.items firstObject];

        if ([feedBackDetails.Result integerValue] == [SUCCESS_RESPONCE integerValue]) {
            // get questions
            patientVisitId = feedBackDetails.PatientVisitId;
            [self getQuestions:@14];
            
        }
        
        
    }];
    
}

- (void)getQuestions:(NSNumber *)visitId{
    
    UMGBasicHttpBinding_IPrimeAppService* service = [[UMGBasicHttpBinding_IPrimeAppService alloc]init];
    service.EnableLogging = YES;
    [service GetQuestionAnswersForFeedbackAsyncWithBlock:@14 QuestionOrder:@1 TransType:@"GET_ALL_QUESTION_ANSWERS" QuestionType:@"option" __handler:^(id obj) {
        UMGArrayOfPatientFeedbackDetails *res = (UMGArrayOfPatientFeedbackDetails *)obj;
        if (res.count > 0) {
            questionsArray = [[NSMutableArray alloc] init];
            for (UMGPatientFeedbackDetails *detail in res) {
                [questionsArray addObject:detail];;
            }
            
//            display questions
            dispatch_async(dispatch_get_main_queue(), ^{
                [self displayQuestions];
            });
        }
    }];
    
}

- (void)displayQuestions{
    if (displayedQuestions.count > questionsArray.count) {
        //update servay status to server.
        [self showThankYouView];
        return;
    }
    UMGPatientFeedbackDetails *firstQuestion = [questionsArray objectAtIndex:displayedQuestions.count>0?displayedQuestions.count:0];
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    [dictionary setObject:firstQuestion forKey:@"question"];
    if ([firstQuestion.QuestionType isEqualToString:@"Option"]) {
        _multipleChoiceView.hidden = NO;
        _commentView.hidden = YES;
        _thankYouView.hidden = YES;
        _questionLabel.text = [firstQuestion getQuestion];
        NSInteger buttonIndex = 0;
        NSMutableArray *answers = [[NSMutableArray alloc] init];
        for (UMGPatientFeedbackDetails *answer  in questionsArray) {
        
            if (firstQuestion.QuestionId == answer.QuestionId) {
                UIButton *button = [buttonArray objectAtIndex:buttonIndex];
                [button setTitle:answer.Answer forState:UIControlStateNormal];
                button.tag = buttonIndex;
                button.hidden = NO;
                buttonIndex++;
                [answers addObject:answer];
                [displayedQuestions addObject:answer];
            }
        }
        [dictionary setObject:answers forKey:@"answers"];
        [answersArray addObject:dictionary];
//        previousQuestion = currentQuestion;
        currentQuestion = dictionary;
        [SVProgressHUD dismiss];
    }
    else{
        _commentView.hidden = NO;
        _multipleChoiceView.hidden = YES;
        _thankYouView.hidden = YES;
        _commentQuestionLabel.text = firstQuestion.Question;
        [dictionary setObject:@[firstQuestion] forKey:@"answers"];
        currentQuestion = dictionary;
        [SVProgressHUD dismiss];
    }
    
}
- (IBAction)answerButton:(UIButton *)sender {
    
    NSInteger tag = sender.tag;
    [self answeratIndex:tag];
}

- (void)answeratIndex:(NSInteger)index{
    NSMutableArray *answers = [currentQuestion objectForKey:@"answers"];
//    UMGPatientFeedbackDetails *question = [currentQuestion objectForKey:@"question"];
    UMGPatientFeedbackDetails *answer = [answers objectAtIndex:index];
//    submit answer
//    on success display next question.
    [SVProgressHUD showWithStatus:@"Loading..."];
    UMGBasicHttpBinding_IPrimeAppService* service = [[UMGBasicHttpBinding_IPrimeAppService alloc]init];
    service.EnableLogging = YES;
    [service AddPatientSurveyDetailsAsyncWithBlock:feedbackId PatientVisitId:patientVisitId QuestionId:answer.QuestionId AnswerId:answer.AnswerId Comments:@"" TransType:@"ADD" __handler:^(id obj) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //[SVProgressHUD dismiss];
            [self displayQuestions];
        });
    }];
}

- (IBAction)submitAction:(id)sender {
    
    UMGPatientFeedbackDetails *question = [currentQuestion objectForKey:@"question"];
    [SVProgressHUD showWithStatus:@"Loading..."];
    UMGBasicHttpBinding_IPrimeAppService* service = [[UMGBasicHttpBinding_IPrimeAppService alloc]init];
    service.EnableLogging = YES;
    [service AddPatientSurveyDetailsAsyncWithBlock:feedbackId PatientVisitId:patientVisitId QuestionId:question.QuestionId AnswerId:@0 Comments:_commentField.text TransType:@"ADD" __handler:^(id obj) {
        UMGArrayOfPatientFeedbackDetails *res = (UMGArrayOfPatientFeedbackDetails *)obj;
        UMGPatientFeedbackDetails *feedBackDetails = [res.items firstObject];
        
        if ([feedBackDetails.Result integerValue] == [SUCCESS_RESPONCE integerValue]) {
            // update survay status
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [self showThankYouView];
            });
            
                    }
    }];
}

- (void)showThankYouView{
    _thankYouView.hidden = NO;
    _commentView.hidden = YES;
    _multipleChoiceView.hidden = YES;
}

- (IBAction)finishButtonClicked:(id)sender {
    [SVProgressHUD showWithStatus:@"Loading..."];
    [self updateSurveyCompleted];

}

- (void)updateSurveyCompleted{
    UMGBasicHttpBinding_IPrimeAppService* service = [[UMGBasicHttpBinding_IPrimeAppService alloc]init];
    service.EnableLogging = YES;
    [service UpdateSurveyCompletedAsyncWithBlock:patientVisitId DateOfFeedback:[NSDate date] TransType:@"MODIFY" __handler:^(id obj) {
        UMGArrayOfPatientFeedbackDetails *res = (UMGArrayOfPatientFeedbackDetails *)obj;
        UMGPatientFeedbackDetails *feedBackDetails = [res.items firstObject];
        
        if ([feedBackDetails.Result integerValue] == [SUCCESS_RESPONCE integerValue]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self showSurveyCompletedAlert:@"Survey completed successfully" cancel:NO];
            });
        }
    }];
}

- (void)showSurveyCompletedAlert:(NSString *)message cancel:(BOOL)cancel{
    [SVProgressHUD dismiss];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Alert!" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    if (cancel) {
        UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:cancelAction];
    }
    
    [alertController addAction:ok];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//
//  AboutAppViewController.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 01/08/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"

@interface AboutAppViewController : BaseViewController
@property (strong,nonatomic) UIPageViewController *pageViewController;

@end

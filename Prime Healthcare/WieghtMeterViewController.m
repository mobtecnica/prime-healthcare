//
//  WieghtMeterViewController.m
//  Prime Healthcare
//
//  Created by macbook pro on 8/11/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "WieghtMeterViewController.h"
#import "SFGaugeView.h"

@interface WieghtMeterViewController ()
@property (weak, nonatomic) IBOutlet SFGaugeView *weightMeter;

@end

@implementation WieghtMeterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.weightMeter.bgColor = [UIColor clearColor];//[UIColor colorWithRed:0/255.0 green:124/255.0 blue:205/255.0 alpha:1];
    self.weightMeter.needleColor = [UIColor colorWithRed:0/255.0 green:39/255.0 blue:64/255.0 alpha:1];
    self.weightMeter.hideLevel = YES;
    self.weightMeter.minImage = @"";
    self.weightMeter.maxImage = @"";
    self.weightMeter.autoAdjustImageColors = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

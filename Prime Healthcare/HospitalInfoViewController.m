//
//  HospitalInfoViewController.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 25/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "HospitalInfoViewController.h"
#import "HospitalTableViewCell.h"
#import "Header.h"
#import "BETHospitalDetails.h"
#import "HospitalDetailsViewController.h"


@interface HospitalInfoViewController ()<UITableViewDelegate,UITableViewDataSource,BETSoapServiceResponse,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *findHospital;

@end

@implementation HospitalInfoViewController{
    NSMutableArray *hospitalDataList;
    NSArray *sortedArray;
    BETArrayOfHospitalDetails *hospitalList;
    NSMutableArray *annotationArray;
    BOOL isSortArray;

}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];

    hospitalDataList=[NSMutableArray new];

    [self getHosptitalList];
    
    locationManager=[CLLocationManager new];
    locationManager.delegate = self;
    _mapview.delegate=self;
//    _mapview.showsUserLocation = YES;
//    _mapview.userTrackingMode =
    NSString * osVersion = [[UIDevice currentDevice] systemVersion];
    if ([osVersion floatValue]>= 8.0 ) {
        [locationManager requestAlwaysAuthorization]; //Requests permission to use location services whenever the app is running.
      [locationManager requestWhenInUseAuthorization]; //Requests permission to use location services while the app is in the foreground.
    }
  [locationManager startUpdatingLocation];
  //  [self setCustomLocation];
   

   
}
-(void)viewDidAppear:(BOOL)animated{
  

}
-(void)setUpView{
    
    //settinf color for place holder
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Prime Hospital" attributes:@{ NSForegroundColorAttributeName : [UIColor whiteColor] }];
    self.findHospital.attributedPlaceholder = str;
    
    
    //search bar icon
    _findHospital.rightViewMode = UITextFieldViewModeAlways;
    UIImageView *searchImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search"]];
    [_findHospital setRightView:searchImage];
    
    
    _mapview.layer.cornerRadius=5.0f;
    _SearchImage.image=[UIImage ipMaskedImageNamed:@"search" color:[UIColor whiteColor]];
    
    self.title=@"Hospital Info";
    
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

#pragma mark- textfeild delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if ([textField.text isEqualToString:@""]) {
        
        isSortArray=NO;
        [self.view endEditing:YES];

        [_ListTableview reloadData];
//        call annotation
        [self addAnnotationOnMap:hospitalDataList];
        return YES;
    }
    else{
        [self filterContentForSearchText:textField.text scope:@""];
        return YES;
    }
}
#pragma mark - Tableview delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (isSortArray) {
      return   sortedArray.count;
    }
    else{
     return    hospitalDataList.count;
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [SVProgressHUD dismiss];
    HospitalTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cellid"];
    BETHospitalDetails *hospital;
    if (!isSortArray) {

    hospital=[hospitalDataList objectAtIndex:indexPath.row];
    }
    else{
        hospital=[sortedArray objectAtIndex:indexPath.row];

    }
    //lat-25.2707763-long-55.32221179999999


    cell.NameLabel.text=hospital.ClinicName;
    cell.AddressLabel.text=hospital.StreetName;
    cell.ImageOutlet.layer.cornerRadius=20.0f;
    [cell.ImageOutlet sd_setImageWithURL:[NSURL URLWithString:hospital.HospitalPhoto] placeholderImage:[UIImage imageNamed:@"placeholder_square"]];
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"infoSegue" sender:self];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    //
    
    if ([segue.identifier isEqualToString:@"infoSegue"]) {
        HospitalDetailsViewController *vc=segue.destinationViewController;

        NSIndexPath *selectedIndex = [_ListTableview indexPathForSelectedRow];
        BETHospitalDetails *hospital1;
        if (!isSortArray) {
            
            hospital1=[hospitalDataList objectAtIndex:selectedIndex.row];
        }
        else{
            hospital1=[sortedArray objectAtIndex:selectedIndex.row];
            
        }
        vc.hospital=hospital1;
    
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - SOAP REQUEST

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    CGFloat offsetY = scrollView.contentOffset.y;
//    if (offsetY > self.view.frame.origin.y - 170) {
//        if (offsetY >= 44) {
//            [self setNavigationBarTransformProgress:1];
//        } else {
//            [self setNavigationBarTransformProgress:(offsetY / 170)];
//        }
//    } else {
//        [self setNavigationBarTransformProgress:0];
//        self.navigationController.navigationBar.backIndicatorImage = [UIImage new];
//    }
//}

//- (void)setNavigationBarTransformProgress:(CGFloat)progress
//{
//    [self.navigationController.navigationBar lt_setTranslationY:(-44 * progress)];
////    [self.navigationController.navigationBar lt_setElementsAlpha:(1-progress)];
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)getHosptitalList{
    //CALLING WEBSERVICE
    
    if ([self isInternetConnection]) {
        [SVProgressHUD showWithStatus:@"Fetching..."];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            BETBasicHttpBinding_IPrimeAppService* service = [[BETBasicHttpBinding_IPrimeAppService alloc]init];
            service.EnableLogging=TRUE;
            [service GetAllHospitalDetailsAsync:self];
        });
    }
    else{
        [self showNoInternetAlert];
    }
   
}
- (IBAction)LeftbuttonAction:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

#pragma mark- Soap Responce
-(void)onSuccess:(id)value methodName:(NSString *)methodName{
    dispatch_async(dispatch_get_main_queue(), ^{

        if ([methodName isEqualToString:@"GetAllHospitalDetails"]) {
        
        hospitalList=(BETArrayOfHospitalDetails *)value;
            
            for (BETHospitalDetails *hospital in hospitalList.items) {
                [hospitalDataList addObject:hospital];
            }
        [_ListTableview reloadData];
//            call annotation method
            [self addAnnotationOnMap:hospitalDataList];
    }
    else{
        
    }
    });
}
-(void)onError:(NSError *)error{
    [SVProgressHUD dismiss];
 
}




- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.ClinicName contains[cd] %@",searchText];
     NSArray *filteredArray = [hospitalDataList filteredArrayUsingPredicate:bPredicate];
   
    sortedArray=[NSArray arrayWithArray:filteredArray];
    isSortArray=YES;
    [self.view endEditing:YES];
    [_ListTableview reloadData];
//    call annotation method
    [self addAnnotationOnMap:sortedArray];

    NSLog(@"%@",filteredArray);

}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    isSortArray=NO;
    [self.view endEditing:YES];

    [_ListTableview reloadData];
//    call annotation
    [self addAnnotationOnMap:hospitalDataList];

}

#pragma mark - MAP Delegates
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        
        //      selectedCoordinate = view.annotation.coordinate;
        return nil;
    }
    else
    {
        MKAnnotationView *pinView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"pinID"];
        NSLog(@"pin map");
        if(pinView == nil)
        {
            pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pinID"];
            
        }
        pinView.image=[UIImage imageNamed:@"loc."];
        //       MKAnnotationView *view=[MKAnnotationView alloc]ini
        //   pinView.image = [UIImage imageNamed:@"pin"];
        pinView.canShowCallout = YES;
        pinView.enabled=YES;
        //    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        
        return pinView;
    }
}

- (void)addAnnotationOnMap:(NSArray *)collection{
    
    if ([annotationArray count] > 0) {
        [_mapview removeAnnotations:annotationArray];
    }
    
    
    for (BETHospitalDetails *hospital in collection) {
        CLLocation *LocationAtual = [[CLLocation alloc] initWithLatitude:[hospital.LatitudeLocation floatValue] longitude:[hospital.LongtitudeLocation floatValue]];
        
        MKPointAnnotation *pointt = [[MKPointAnnotation alloc] init];
        pointt.coordinate = LocationAtual.coordinate;
        pointt.title=hospital.ClinicName;
        pointt.subtitle = hospital.StreetName;
        [_mapview addAnnotation:pointt];
        [annotationArray addObject:pointt];
        [_mapview setCenterCoordinate:pointt.coordinate animated:YES];
        MKCoordinateRegion region = [_mapview regionThatFits:MKCoordinateRegionMakeWithDistance(pointt.coordinate, 400, 400)];
        [self.mapview setRegion:[self.mapview regionThatFits:region] animated:YES];
    }
//    [_mapview showAnnotations:annotationArray animated:YES];
    [self makeAnnotationsVisible];
    
}

- (void)makeAnnotationsVisible{
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in _mapview.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
//    MKMapPoint annotationPoint = MKMapPointForCoordinate(_mapview.userLocation.coordinate);
//    MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
//    zoomRect = MKMapRectUnion(zoomRect, pointRect);
    
   [_mapview setVisibleMapRect:zoomRect edgePadding:UIEdgeInsetsMake(20, 10, 20, 10) animated:YES];
}

@end

//
//  MyInformationHomeViewController.m
//  Prime Healthcare
//
//  Created by macbook pro on 8/8/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "MyInformationHomeViewController.h"
#import "MyInformationViewController.h"
#import "CMEBasicHttpBinding_IPrimeAppService.h"

#import "Header.h"

#define BTN_PATIENTS_APPOINMENT 100
#define BTN_PAST_VISIT 101
#define BTN_FUTURE_APPOINMENT 102
#define BTN_FAV_DOCTOR 103
#define BTN_MED_DETAILS 104
#define BTN_LAB_RESULTS 105
#define BTN_LOYALITY_PATIENTS 106


#define CELL_WIDTH 100
#define CELL_SPACING 20
#define COLLECTIONVIEW_WIDTH 4000


@interface MyInformationHomeViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,CMESoapServiceResponse>{
    
    UserProfile *userProfile;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewContent;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *patientID;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumbr;
@property (weak, nonatomic) IBOutlet UICollectionView *userCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *menuCollectionView;
@property (weak, nonatomic) IBOutlet UIView *loyalityButton;

@end

@implementation MyInformationHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    userProfile = [UserProfile getUserProfile];
    [self setUpView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpView{
    self.title=@"My Information";
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    _profileImage.layer.cornerRadius=_profileImage.frame.size.height/2;
    _profileImage.clipsToBounds=YES;
//    _profileImage sd_setImageWithURL:[NSURL URLWithString:userProfile.] placeholderImage:<#(UIImage *)#>
    _name.text = userProfile.patientName;
    _patientID.text = userProfile.regNo;
    _mobileNumbr.text = userProfile.mobileNo;
    
    
    if (![userProfile.isLoyal boolValue]) {
        [_loyalityButton setHidden: YES];
    }
    else{
        [_loyalityButton setHidden:NO];
    }
    
}
#pragma mark - Collectiov View Delegates
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 1;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger rows=[collectionView numberOfItemsInSection:indexPath.section];
    
    if (indexPath.row==rows-1) {
        UICollectionViewCell *footerCell=[collectionView dequeueReusableCellWithReuseIdentifier:@"footer" forIndexPath:indexPath];
        return footerCell;
    }
    
    UICollectionViewCell *footerCell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellID" forIndexPath:indexPath];
    return footerCell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(100 , 100);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    NSInteger _numberOfCells = [self collectionView:collectionView numberOfItemsInSection:section];
    NSInteger viewWidth = CGRectGetWidth(self.view.frame);
    NSInteger totalCellWidth = CELL_WIDTH * _numberOfCells;
    NSInteger totalSpacingWidth = CELL_SPACING * (_numberOfCells -1);
    
    float leftInset = (viewWidth - (totalCellWidth + totalSpacingWidth)) / 2;
    float rightInset = leftInset;
    
    return UIEdgeInsetsMake(0, leftInset, 0, rightInset);
}

#pragma mark - Button Action
- (IBAction)buttonActions:(id)sender {
    
    UIButton *btn=(UIButton *)sender;
    [self.view bringSubviewToFront:btn];
    
    if (btn.tag==BTN_FUTURE_APPOINMENT) {
        // Future appointment
        [self performSegueWithIdentifier:@"FutureAppintmentSegue" sender:btn];
    }
    if (btn.tag == BTN_FAV_DOCTOR) {
        // Favarite doctor FavoritDoctorSegue
        [self performSegueWithIdentifier:@"FavoritDoctorSegue" sender:btn];
    }
    if (btn.tag == BTN_PAST_VISIT) {
        // Past visit
        [self performSegueWithIdentifier:@"PastVisitSegue" sender:btn];
    }
    if (btn.tag==BTN_PATIENTS_APPOINMENT){
        // Patient information
        [self performSegueWithIdentifier:@"infoDetail" sender:btn];
    }
    if (btn.tag==BTN_LOYALITY_PATIENTS){
        // Loyality patient
        
    }
    if (btn.tag==BTN_LAB_RESULTS){
        // Lab result
    }
    
    

}
- (IBAction)leftMenu:(id)sender {
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];

}

-(void)onSuccess:(id)value methodName:(NSString *)methodName{
  
    NSLog(@"%@", value);
}
-(void)onError:(NSError *)error{
    
    NSLog(@"%@", error);
 
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"infoDetail"]) {
        
        
    }
}


@end

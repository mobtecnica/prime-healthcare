//
//  AboutContentViewController.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 01/08/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "AboutContentViewController.h"

@interface AboutContentViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *contentPhoto;
@property (weak, nonatomic) IBOutlet UILabel *content;
@end

@implementation AboutContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _ControllerIndicator.currentPage=_pageIndex;
    _content.text=_contentValue;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

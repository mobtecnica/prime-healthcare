//
//  FindDoctorViewController.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 01/08/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"

@interface FindDoctorViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *allClinicText;
@property (weak, nonatomic) IBOutlet UITextField *doctorext;
@property (weak, nonatomic) IBOutlet UITextField *allDepartmentText;

@end

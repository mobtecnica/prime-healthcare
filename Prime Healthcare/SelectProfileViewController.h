//
//  SelectProfileViewController.h
//  Prime Healthcare
//
//  Created by macbook pro on 8/23/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "BaseViewController.h"
#import "ASTUserRegistrationRequest.h"
@class SelectProfileViewController;

@protocol SelectProfileDelegate <NSObject>
@required
-(void)selectedProfileWithPatient:(ASTUserRegistrationRequest *)patient from:(SelectProfileViewController *)vc;
@end

@interface SelectProfileViewController : BaseViewController

@property(nonatomic,strong) NSArray *patientsList;
@property (strong, nonatomic) id <SelectProfileDelegate>delegate;
@end

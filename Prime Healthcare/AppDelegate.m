//
//  AppDelegate.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 18/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "AppDelegate.h"
#import "MFSideMenu.h"
#import "CMEBasicHttpBinding_IPrimeAppService.h"
#import "SplashViewController.h"

@import Firebase;
@interface AppDelegate ()<CMESoapServiceResponse>{
    FIRDatabaseReference *ref;
    NSString *refreshedToken;
   
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    BOOL isLoggedIn =[[NSUserDefaults standardUserDefaults] boolForKey:@"isLogged"];

    if (!isLoggedIn) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        MFSideMenuContainerViewController *container = [storyboard instantiateViewControllerWithIdentifier:@"MenuContainerViewController"];
        self.window.rootViewController = container;
        NSLog(@"hhh %@",[self.window.rootViewController class]);
        UINavigationController *destNav = [storyboard instantiateViewControllerWithIdentifier:@"HomeMainNavController"];
        UITableViewController *sideMenu = [storyboard instantiateViewControllerWithIdentifier:@"sidemenuController"];
        [container setCenterViewController:destNav];
        [container setLeftMenuViewController:sideMenu];
        [container setRightMenuViewController:nil];
        
        //Installing Fire Base
        [self deviceInitialSubscription];
        [FIRApp configure];
        ref= [[FIRDatabase database] reference];
        


    }
    else{
       
        
        self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
        SplashViewController *homeVc = [[SplashViewController alloc]initWithNibName:@"SplashViewController" bundle:nil];
        //UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:homeVc];
        self.window.rootViewController = homeVc;

    }

    return YES;
    
    
    //[self deviceInitialSubscription];
    // Override point for customization after application launch.
}
-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
//    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
//    SplashViewController *homeVc = [[SplashViewController alloc]initWithNibName:@"SplashViewController" bundle:nil];
//    //UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:homeVc];
//    self.window.rootViewController = homeVc;
    return YES;

    
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - DEVICE INITIAL SUBSCRIPTION

-(void)deviceInitialSubscription{
    //For getting the FCM Token -- this token will be used on some other SOAP service
    refreshedToken= [[FIRInstanceID instanceID] token];
    
    [[NSUserDefaults standardUserDefaults]setObject:refreshedToken forKey:@"FCM_TOKEN"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    //uuuid
     NSString *uuid = [[NSUUID UUID] UUIDString];
    uuid = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
    [[NSUserDefaults standardUserDefaults]setObject:uuid forKey:@"DEVICE_ID"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    CMEBasicHttpBinding_IPrimeAppService* service = [CMEBasicHttpBinding_IPrimeAppService new];
    
    [service DeviceInitialSubscriptionAsync:uuid FCMToken:refreshedToken DeviceType:@"1" TransType:@"ADD_INITIAL_SUBSCRIPTION" __target:self];
}

-(void)onSuccess:(id)value methodName:(NSString *)methodName{
    
    NSLog(@"deviceInitialSubscription---------%@",value);
    NSLog(@"deviceInitialSubscription---------%@",methodName);

}
-(void)onError:(NSError *)error{
    
    NSLog(@"");
}
#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "mobtecnica.Prime_Healthcare" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Prime_Healthcare" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Prime_Healthcare.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end

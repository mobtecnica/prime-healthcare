//
//  BaseViewController.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 22/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"

@interface BaseViewController : UIViewController

// Common methods.
-(void)showAlert:(NSString *)title message:(NSString *)value;
-(void)showNoInternetAlert;
-(BOOL)isInternetConnection;
-(void)loadSideMenu;
-(UIBarButtonItem *)setUpBackButton;

@end

//
//  AboutAppViewController.m
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 01/08/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "AboutAppViewController.h"
#import "Header.h"

@interface AboutAppViewController ()<UIPageViewControllerDelegate,UIPageViewControllerDataSource>

@end

@implementation AboutAppViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
   
    
    // Do any additional setup after loading the view.
}
-(void)setUpView{
    
    //other modifications
    self.navigationItem.title=@"About App";
    
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"pageviewID"];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    
    
    AboutContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    //    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    
    //removing the blank space
    NSArray *subviews = self.pageViewController.view.subviews;
    UIPageControl *thisControl = nil;
    for (int i=0; i<[subviews count]; i++) {
        if ([[subviews objectAtIndex:i] isKindOfClass:[UIPageControl class]]) {
            thisControl = (UIPageControl *)[subviews objectAtIndex:i];
        }
    }
    
   // [thisControl  removeFromSuperview];
    self.pageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+40);
    
   

}
#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((AboutContentViewController *) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}
//The above methods are very straightforward. First, we get the current page index. Depending the method, we simply increase/decrease the index number and return the view controller to display. Of course, we have to verify if we have reached the boundaries of the pages and return nil in that case.

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((AboutContentViewController*) viewController).pageIndex;
//    
    if (index == NSNotFound) {
        return nil;
    }
//
    index++;
    if (index == 3) {
        return nil;
    }
    else
        return [self viewControllerAtIndex:index];
}


- (AboutContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    
    // Create a new view controller and pass suitable data.
    AboutContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"pageContentViewController"];
    pageContentViewController.pageIndex = index;
    pageContentViewController.contentValue=[NSString stringWithFormat:@"page %d",(int)index];
    return pageContentViewController;
}


- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return 4;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)LeftmenuAction:(id)sender {
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

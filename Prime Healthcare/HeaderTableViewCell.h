//
//  HeaderTableViewCell.h
//  Prime Healthcare
//
//  Created by macbook pro on 8/30/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *logBtn;
@property (weak, nonatomic) IBOutlet UIView *baseView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImg;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalConstrain;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *btnWidthConstrain;

@end

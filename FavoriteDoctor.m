//
//  FavoriteDoctor.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 01/10/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "FavoriteDoctor.h"

#import "AppDelegate.h"



#define kEntityName @"Doctors"

@implementation FavoriteDoctor

// Insert code here to add functionality to your managed object subclass
+(void)saveDoctor:(CMEDoctorDetails *)details{
    
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    FavoriteDoctor *doctor = [NSEntityDescription insertNewObjectForEntityForName:kEntityName inManagedObjectContext:context];
    doctor.doctorId = details.DoctorId;
    doctor.doctorName = details.DoctorName;
    doctor.doctorPhoto = details.DoctorPhoto;
    doctor.qualification = details.Qualification;
    doctor.discriptionText = details.Description;
    doctor.clinicId = [NSString stringWithFormat:@"%@",details.ClinicId];
    doctor.clinicName = details.ClinicName;
    doctor.departmentId = [NSString stringWithFormat:@"%@",details.DepartmentId];
    doctor.departmentName = details.DepartmentName;
    doctor.email = details.Email;
    doctor.discriptionText = details.Description;
    doctor.favoriteAdded = [NSNumber numberWithBool:NO];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save user Doctor details! %@ %@", error, [error localizedDescription]);
    }
    else
    {
        NSLog(@"User user doctor details saved Successfully");
    }
    
}


+ (NSArray *)getAllDoctors{
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *myContext = [delegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:kEntityName];
    //    [request setFetchLimit:1];
    NSError *error = nil;
    NSArray *results = [myContext executeFetchRequest:request error:&error];
    
    
    if (error != nil) {
        
        NSLog(@"Failed to get appointment details! %@! %@!",error, [error localizedDescription]);
    }
    else {
        
    }
    return results;
}

+ (void)deleteAllDoctors{
    
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *myContext = [delegate managedObjectContext];
    NSPersistentStoreCoordinator *myPersistentStoreCoordinator = [delegate persistentStoreCoordinator];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:kEntityName];
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
    
    NSError *deleteError = nil;
    if ([myPersistentStoreCoordinator executeRequest:delete withContext:myContext error:&deleteError]) {
        NSLog(@"Deleted appointment details Successfully");
    }
    else{
        NSLog(@"Can't delete appointment details! %@! %@",deleteError, deleteError.localizedDescription);
        
    }
}

+ (NSArray *)getAllFavoriteDoctors{
    
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *myContext = [delegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:kEntityName];
    //    [request setFetchLimit:1];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"favoriteAdded == YES"];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *results = [myContext executeFetchRequest:request error:&error];
    
    
    if (error != nil) {
        
        NSLog(@"Failed to get appointment details! %@! %@!",error, [error localizedDescription]);
    }
    else {
        
    }
    return results;
}

+ (void)addFavoriteDoctor:(NSString *)doctorID add:(BOOL) add{
    
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *myContext = [delegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:kEntityName];
    [request setFetchLimit:1];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"doctorId == %@", doctorID];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [myContext executeFetchRequest:request error:&error];
    if (results.count > 0) {
        FavoriteDoctor * doctor = [results firstObject];
        doctor.favoriteAdded = [NSNumber numberWithBool:add];
        [myContext save:&error];
    }
    
    if (error != nil) {
        
        NSLog(@"Failed to get appointment details! %@! %@!",error, [error localizedDescription]);
    }
    else {
        
    }
    
}

+ (BOOL)updateDoctor{
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *myContext = [delegate managedObjectContext];
    NSError *error = nil;
    [myContext save:&error];
    if (error == nil) {
        return  YES;
    }
    else{
        return NO;
        
    }
}

+ (FavoriteDoctor *)getDoctor:(NSString *)doctorID{
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *myContext = [delegate managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:kEntityName];
    [request setFetchLimit:1];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"doctorId == %@", doctorID];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [myContext executeFetchRequest:request error:&error];
    FavoriteDoctor * doctor = nil;
    if (results.count > 0) {
        doctor = [results firstObject];
    }
    return doctor;
}

@end

//
//  UserProfile+CoreDataProperties.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 29/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "UserProfile.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserProfile (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *dateOfBirth;
@property (nullable, nonatomic, retain) NSString *email;
@property (nullable, nonatomic, retain) NSString *emiratesID;
@property (nullable, nonatomic, retain) NSNumber *isChronic;
@property (nullable, nonatomic, retain) NSNumber *isLoyal;
@property (nullable, nonatomic, retain) NSString *mobileNo;
@property (nullable, nonatomic, retain) NSString *patientName;
@property (nullable, nonatomic, retain) NSString *regNo;

@end

NS_ASSUME_NONNULL_END

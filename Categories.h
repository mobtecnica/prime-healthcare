//
//  Categories.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 29/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#ifndef Categories_h
#define Categories_h

#import "UIImage+ColorMask.h"
#import "UIColor+Hex.h"
#import "UINavigationBar+Awesome.h"

#endif /* Categories_h */

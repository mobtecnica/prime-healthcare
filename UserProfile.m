//
//  UserProfile.m
//  Prime Healthcare
//
//  Created by Mobile Developer on 29/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "UserProfile.h"
#import "AppDelegate.h"

#define kEntityName @"UserProfile"


@implementation UserProfile

// Insert code here to add functionality to your managed object subclass
+ (void)saveUserDetails:(USLUserProfile *)profile{
    
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [delegate managedObjectContext];
    
    UserProfile *userProfile = [NSEntityDescription insertNewObjectForEntityForName:kEntityName inManagedObjectContext:context];
    userProfile.regNo = profile.RGNo;
    userProfile.mobileNo = profile.MobileNo;
    userProfile.dateOfBirth = profile.DateOfBirth;
    userProfile.email = profile.Email;
    userProfile.emiratesID = profile.EmiratesID;
    userProfile.isChronic = profile.IsChronic;
    userProfile.isLoyal = profile.IsLoyal;
    userProfile.patientName = profile.PatientName;
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save user profile! %@ %@", error, [error localizedDescription]);
    }
    else
    {
        NSLog(@"User profile saved Successfully");
    }
    
}

+ (void)deleteUserDetails{
    
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *myContext = [delegate managedObjectContext];
    NSPersistentStoreCoordinator *myPersistentStoreCoordinator = [delegate persistentStoreCoordinator];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Car"];
    NSBatchDeleteRequest *delete = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
    
    NSError *deleteError = nil;
    if ([myPersistentStoreCoordinator executeRequest:delete withContext:myContext error:&deleteError]) {
        NSLog(@"Deleted Successfully");
    }
    else{
        NSLog(@"Can't delete userProfile! %@! %@",deleteError, deleteError.localizedDescription);
    }
}

+ (UserProfile *)getUserProfile{
    
    id delegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *myContext = [delegate managedObjectContext];
    UserProfile *profile = nil;
    NSFetchRequest *request = [[NSFetchRequest alloc]initWithEntityName:kEntityName];
    [request setFetchLimit:1];
    NSError *error = nil;
    NSArray *results = [myContext executeFetchRequest:request error:&error];
    
    
    if (error != nil) {
        
        NSLog(@"Failed to get user! %@! %@!",error, [error localizedDescription]);
    }
    else {
        
        //Deal with success
        if (results.count>0) {
            profile = [results firstObject];
        }
    }
    return profile;
}

@end

//
//  FutureAppointments.h
//  Prime Healthcare
//
//  Created by Mobile Developer on 30/09/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "EDRFutureAppointmentDetails.h"

NS_ASSUME_NONNULL_BEGIN

@interface FutureAppointments : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
+ (void)saveFutureAppointment:(EDRFutureAppointmentDetails *)details;
+ (void)deleteAllFutureAppointmentsList;
+ (NSArray *)getAllFutureAppointmentsDetails;
@end

NS_ASSUME_NONNULL_END

#import "FutureAppointments+CoreDataProperties.h"

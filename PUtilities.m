//
//  PUtilities.m
//  Prime Healthcare
//
//  Created by macbook pro on 9/15/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import "PUtilities.h"

@implementation PUtilities
+(void)setUserInfo:(NSDictionary *)userInfo{
    [[NSUserDefaults  standardUserDefaults]setObject:userInfo forKey:@"CURRENT_USER"];
    [[NSUserDefaults standardUserDefaults]synchronize];

}
+(NSDictionary *)getUserInfo{
    NSDictionary *userInfo = [[NSUserDefaults standardUserDefaults]valueForKey:@"CURRENT_USER"];
    NSLog(@"User info : %@", userInfo);
    return userInfo;
}
+(NSString *)getName{
    NSDictionary *userInfo=[self getUserInfo];
    
   return  [userInfo valueForKey:@"name"]?[userInfo valueForKey:@"name"]:@"";
}
+(NSString *)getRegId{
    NSDictionary *userInfo=[self getUserInfo];
       return  [userInfo valueForKey:@"regNo"]?[userInfo valueForKey:@"regNo"]:@"";
}
+(NSString *)getMobile{
    NSDictionary *userInfo=[self getUserInfo];
    
    return  [userInfo valueForKey:@"mobile"]?[userInfo valueForKey:@"mobile"]:@"";
}
+(NSString *)getpassword{
    NSDictionary *userInfo=[self getUserInfo];
    
    return  [userInfo valueForKey:@"password"]?[userInfo valueForKey:@"password"]:@"";

}
@end

//
//  UIColor+Hex.h
//  Prime Healthcare
//
//  Created by Mobtecnica Mac Mini on 26/07/16.
//  Copyright © 2016 Mobtecnica Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)

+ (UIColor*)colorWithHexString:(NSString*)hex;

@end
